#ifndef CONFIG_H
#define CONFIG_H

// Stepper pins
#define DIR 11
#define STEP 12
#define MS1 40
#define MS2 41
#define MS3 42
#define END_STOP 5

// Stepper settings
#define MOTOR_STEPS 200
#define STEPS_PER_MM 64.1025641025641
#define RPM 120
#define MICROSTEPS 16
// Maximum allowed stepper position
#define STEPPER_MAX_POS 100
// Acceleration and deceleration values are always in FULL steps / s^2
#define MOTOR_ACCEL 1000
#define MOTOR_DECEL 1000


#define SERVO_PIN 3
#define SERVO_RPM 50
// Min and max pulse of servo
#define SERVO_MIN 900
#define SERVO_MAX 2100
// The angles of the Servo to perform certain actions
#define SERVO_CLOSED 5
#define SERVO_PEEK 70
#define SERVO_TOUCH 95
#define SERVO_PRESS 106

// NeoPixel pins
#define LED_PIN 13
#define LED_COUNT 94

// Box settings
#define NUM_SWITCHES 5
// The higher the level, the more different actions the box will make
// The higher HOW_FAST_TO_LEVEL_UP, the slower the box gains levels
#define HOW_FAST_TO_LEVEL_UP 1

// The input pins of the switches
const uint32_t switch_pins[] = {
    10,
    9,
    8,
    7,
    6,
};

// The positions of the switches in mm
const double switch_positions[] = {
    0,
    37,
    76,
    115,
    153,
};


#endif