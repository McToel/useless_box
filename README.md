# NwT Projekt Useless Box

Code: https://gitlab.com/McToel/useless_box

### Funktionen des Programms
Ziel des Programms ist es alle angeschalteten Schalter auszuschalten. Solange ein schalter an ist, ist das Licht in der Box an.
Da der Arduino keine Mutlitasking / Threading unterstützt, müssen alle zu erledigen Aufgaben in einem non-blocking Modus erledigt werden. Befehle wie `delay()` können nicht verwendet werden.

### Aktionen
Damit die Verwendung der Box spannender ist, soll die Box nicht immer genau gleich reagieren. Dafür haben wir verschiedene Aktionen definier. Die Box kann entweder den Schalter umlegen (`PRESS`), den Schalter nur berühren (`TOUCH`), nur die Box etwas öffnen (`PEEK`), gar nichts tun (`WAIT`) oder den Schalter verfehlen (`MISS`). Am Ende muss immer der Schalter umgelegt werden. Wir generieren Aktionsketten, dessen Ende immer `PRESS` ist. Nicht jede Aktion ist nach jeder anderen möglich. Z.B. nach `TOUCH` soll `PEEK` nicht mehr vorkommen. Da wir die Aktionen in Zweierpotenzen definiert haben, können wir bitweise Operatoren verwenden, um die zukünftig erlaubten Aktionen zu berechnen.

### Levels
Wird die Box angeschaltet, so soll sie am Anfang einfach die gedrückten Schalter zurückdrücken. Später soll die Box auch mal Pausen machen oder den Schalter erstmal nur berühren, ohne ihn direkt zu drücken. Dafür verwenden wir Levels. Das Level ist abhänging von der Anzahl der bereits umgelegten Schaltern. Das maximale Level ist die Anzahl der verschiedenen Aktionen, die die Box machen kann. Wie schnell die Box ein neues Level erreicht, kann durch `HOW_FAST_TO_LEVEL_UP` bestimmt werden.

### Servo und NeoPixel
Um die NeoPixel anzusteuern wird ein sehr genaues Timing benötigt. Um dies zu erreichen, schaltet die Adafruit NeoPixel Library alle Interrupts aus, während Daten an die Pixel geschrieben werden. Da die Servo Library aber auf Interrupts basiert, funktionieren diese beide nicht miteinander. Anstelle der Adafruit NeoPixel Library verwenden wir daher FastLED. FastLED hat eigentlich das gleiche Problem, aber da der Arduino Due so schnell ist, schaltet FastLED für den Due zwischendurch immer wieder die Interrups an, wodurch die Servobibliotek einigermaßen funktioniert.

### Verwendete Bibliotheken
- arduino-timer: https://github.com/contrem/arduino-timer
- StepperDriver: https://github.com/laurb9/StepperDriver
- Servo: https://github.com/arduino-libraries/Servo
- FastLED: https://github.com/FastLED/FastLED