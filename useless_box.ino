#include <Arduino.h>
#include <Servo.h>
#include <FastLED.h>
#include "arduino-timer.h"
#include "A4988.h"
#include "MyStepper.h"
#include "config.h"


Timer<10> timer; // = timer_create_default();

MyStepper stepper(
    STEPS_PER_MM, -1, END_STOP, MOTOR_STEPS,
    RPM, MOTOR_ACCEL, MOTOR_DECEL, STEPPER_MAX_POS,
    DIR, STEP, MS1, MS2, MS3);

// Give a nice name to the Servo
Servo hanz;

CRGB leds[LED_COUNT];


/*
 * Functions and variables to controll the servo's speed
 */
int speed_servo_goal = 0;
int speed_servo_position = 0;
void speed_servo_write(int pos) {
    speed_servo_goal = pos;
}
bool speed_servo_step(void *){
    // Serial.println("Stepping");
    if (speed_servo_position < speed_servo_goal) {
        hanz.write(++speed_servo_position);
    }
    else if (speed_servo_position > speed_servo_goal) {
        hanz.write(--speed_servo_position);
    }
    return true;
}
void speed_servo_init() {
    hanz.attach(SERVO_PIN, 900, 2100);
    hanz.write(SERVO_CLOSED);
    timer.every((60000) / (SERVO_RPM * 360), speed_servo_step);
}


volatile uint_fast8_t switch_states[] = {
    1,
    1,
    1,
    1,
    1,
};

// Could have been used for a rotating switch but its also handy
// as with this it does not matter how the switches are connected
uint_fast8_t switch_state_inverted[] = {
    1,
    1,
    1,
    1,
    1,
};

uint_fast32_t switched_count = 0;

uint32_t currently_pressing = NUM_SWITCHES;

// Posible action (powers of 2 so that each action corresponds
// to a specific bit)
#define PRESS 1
#define WAIT 2
#define TOUCH 4
#define PEEK 8
#define MISS 16

// What actions are allowed after which action
uint_fast8_t after_press = WAIT;
uint_fast8_t after_wait = PRESS | TOUCH | PEEK | MISS;
uint_fast8_t after_touch = PRESS | WAIT;
uint_fast8_t after_peek = PRESS | WAIT | TOUCH | PEEK;
uint_fast8_t after_miss = PRESS | WAIT | PEEK;



uint_fast8_t current_action = 0;
// All actions allowed
uint_fast8_t current_allowed_actions = 0xFF;
bool wait = false;
bool action_finished = false;
uint8_t missed = 0;

/*
 * Depending on the level and the currently allowed actions, choose the
 * next action at random.
 */
uint_fast8_t choose_action(uint_fast8_t general_allowed, int num_moves) {
    int max_action = num_moves / HOW_FAST_TO_LEVEL_UP;
    for (int i = 0; i < 10; i++) {
        switch (random(0, min(max_action, 5))) {
        case 0:
            if (general_allowed & PRESS) {
                Serial.println("Choose PRESS");
                action_finished = false;
                return PRESS;
            }
            break;

        case 1:
            if (general_allowed & WAIT) {
                Serial.println("Choose WAIT");
                action_finished = false;
                return WAIT;
            }
            break;

        case 2:
            if (general_allowed & TOUCH) {
                Serial.println("Choose TOUCH");
                action_finished = false;
                return TOUCH;
            }
            break;

        case 3:
            if (general_allowed & PEEK) {
                Serial.println("Choose PEEK");
                action_finished = false;
                return PEEK;
            }
            break;

        case 4:
            if (general_allowed & MISS) {
                Serial.println("Choose MISS");
                action_finished = false;
                missed = 0;
                return MISS;
            }
            break;
        }
    }
    return 0;
}

/*
 * After choosing a new action, update the allowed actions.
 */
uint_fast8_t update_allowed_actions (uint_fast8_t general_allowed, uint_fast8_t action) {
    switch (action) {
    case PRESS:
        return general_allowed & after_press;
    case WAIT:
        return general_allowed & after_wait;
    case TOUCH:
        return general_allowed & after_touch;
    case PEEK:
        return general_allowed & after_peek;
    case MISS:
        return general_allowed & after_miss;
    
    default:
        break;
    }
}

/*
 * Find the nearest switched switch to the position of the stepper
 */
int nearest_switched_switch() {
    double min_distance = 1e10;
    int min_distance_switch = NUM_SWITCHES;
    for (int i = 0; i < NUM_SWITCHES; i++) {
        if (switch_on(i)) {
            double dist = abs(stepper.get_position_mm() - switch_positions[i]);
            if (dist < min_distance) {
                min_distance = dist;
                min_distance_switch = i;
            }
        }
    }
    return min_distance_switch;
}

/*
 * Create a rainbow pattern on the NeoPixel strip as soon as
 * a switch was swiched on
 */
bool theater_chase_rainbow_nonblocking(void *) {
    if (currently_pressing != NUM_SWITCHES) {
        if (!leds[0]) {
            fill_rainbow(leds, LED_COUNT, beat8(10, 255), 5);
            FastLED.show();
        }
    }
    else if (leds[0]) {
        FastLED.clear();
        FastLED.show();
    }
    return true;
}

/*
 * Measure the state of all switches
 */
void measure_switches() {
    for (uint8_t i = 0; i < NUM_SWITCHES; i++) {
        switch_states[i] = digitalRead(switch_pins[i]);
    }
}

/*
 * Check if a specific switch is pressed
 */
bool switch_on(int pin) {
    return (switch_states[pin] != switch_state_inverted[pin]);
}

/*
 * Set wait to false. To be used with a timeout in order to
 * create a non-blocking delay.
 */
bool stop_wait (void *) {
    wait = false;
    return false;
}

void setup() {
    // Set interrups for input switches
    for (uint8_t i = 0; i < NUM_SWITCHES; i++) {
        attachInterrupt(digitalPinToInterrupt(switch_pins[i]), measure_switches, CHANGE);
    }
    measure_switches();

    Serial.begin(115200);

    // Initialise NeoPixels
    FastLED.addLeds<NEOPIXEL, LED_PIN>(leds, LED_COUNT);
    FastLED.clear();
    FastLED.show();
    FastLED.setBrightness(100);

    stepper.begin();
    Serial.println("Homed");

    speed_servo_init();

    timer.every(100, theater_chase_rainbow_nonblocking);

    // // Usefull for configuring the servo
    // while (true) {
    //     if (Serial.available()) {
    //         // hanz.write(Serial.readStringUntil(';').toInt());
    //         speed_servo_write(Serial.readStringUntil(';').toInt());
    //     }
    //     timer.tick();
    // }
}

void loop() {
    // If no switch is currently switched back of, try to find the nearest
    // switched switch
    if (currently_pressing == NUM_SWITCHES) {
        currently_pressing = nearest_switched_switch();
        if (currently_pressing != NUM_SWITCHES) {
            current_action = choose_action(current_allowed_actions, switched_count);
            current_allowed_actions = update_allowed_actions(current_allowed_actions, current_action);
        }
    }
    // If the switch was turned back off by the box, close hanz
    else if (!switch_on(currently_pressing)) {
        switched_count++;
        speed_servo_write(SERVO_CLOSED);
        currently_pressing = NUM_SWITCHES;
        current_allowed_actions = 0xFF;
        wait = true;
        timer.in(1000, stop_wait);
    }
    // For nonblocking delay
    else if (wait) { /* Serial.println("Wait..."); */ }
    // Find next action, if current_action was finished
    else if (action_finished) {
        current_action = choose_action(current_allowed_actions, switched_count);
        current_allowed_actions = update_allowed_actions(current_allowed_actions, current_action);
    }
    // Perform current action
    else {
        switch (current_action){
            case PRESS:
                stepper.move_to(switch_positions[currently_pressing]);
                if (stepper.at_goal()) {
                    speed_servo_write(SERVO_PRESS);
                    wait = true;
                    timer.in(1000, stop_wait);
                    action_finished = true;
                }
                break;
            case WAIT:
                wait = true;
                timer.in(1000, stop_wait);
                action_finished = true;
                break;
            case TOUCH:
                stepper.move_to(switch_positions[currently_pressing]);
                if (stepper.at_goal()) {
                    speed_servo_write(SERVO_TOUCH);
                    wait = true;
                    timer.in(1000, stop_wait);
                    action_finished = true;
                }
                break;
            case PEEK:
                stepper.move_to(switch_positions[currently_pressing]);
                if (stepper.at_goal()) {
                    speed_servo_write(SERVO_PEEK);
                    wait = true;
                    timer.in(1000, [](void*) -> bool { wait = false; return wait; });
                    action_finished = true;
                }
                break;
            case MISS:
                // MISS has 3 Steps:
                // 1. Move the Stepper to a position where it misses the switch
                // 2. Try to press the switch
                // 3. Close the servo
                if (missed == 0) {
                    if (random(0, 2)) {
                        stepper.move_to(switch_positions[currently_pressing] + 20);
                    }
                    else {
                        stepper.move_to(switch_positions[currently_pressing] - 20);
                    }
                    missed = 1;
                }
                if (stepper.at_goal() && missed == 1) {
                    speed_servo_write(SERVO_PRESS);
                    wait = true;
                    timer.in(500, stop_wait);
                    missed = 2;
                }
                else if (missed == 2) {
                    speed_servo_write(SERVO_CLOSED);
                    wait = true;
                    timer.in(500, stop_wait);
                    action_finished = true;
                }
                break;
        }
    }

    for (int i = 0; i < 50; i++) {
        stepper.nextAction();
        timer.tick();
        delayMicroseconds(100);
    }
}