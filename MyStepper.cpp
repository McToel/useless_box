#include "Arduino.h"
#include "A4988.h"
#include "config.h"
#include "MyStepper.h"

using namespace std;

MyStepper::MyStepper(
    double steps_per_mm, short direction, uint32_t home_switch_pin,
    uint32_t steps, double rpm, double accel, double decel, double max_pos,
    uint32_t dir, uint32_t step, uint32_t ms1, uint32_t ms2, uint32_t ms3
) : A4988(steps, dir, step, ms1, ms2, ms3)
{
    this->steps_per_mm = steps_per_mm;
    this->home_switch_pin = home_switch_pin;
    this->rpm = rpm;
    this->accel = accel;
    this->decel = decel;
    this->direction = direction;
    this->max_pos = max_pos;
    pinMode(this->home_switch_pin, INPUT_PULLUP);
    attachInterrupt(digitalPinToInterrupt(this->home_switch_pin), home_interrupt, CHANGE);
}

/*
 * Returns true, if the stepper reached it's goal
 */
bool MyStepper::at_goal()
{
    if (A4988::getCurrentState() == A4988::STOPPED)
    {
        this->position = this->goal;
        this->_at_goal = true;
    }
    return this->_at_goal;
}

/*
 * Move the stepper to the home switch and than save it's position
 */
void MyStepper::home()
{
    A4988::setRPM(10);
    MyStepper::home_switch = !digitalRead(MyStepper::home_switch_pin);
    while (!this->home_switch)
    {
        A4988::move(-1 * this->direction);
        delay(1);
    }
    
    this->position = 0;
    this->homed = true;
    A4988::setRPM(this->rpm);

    detachInterrupt(digitalPinToInterrupt(this->home_switch_pin));
}

void MyStepper::begin()
{
    A4988::begin(this->rpm, 16);
    A4988::setMicrostep(16);
    A4988::enable();

    this->home();
    A4988::setSpeedProfile(A4988::LINEAR_SPEED, this->accel, this->decel);
}

/*
 * Move the stepper to a specified location in mm
 */
void MyStepper::move_to(double position) {
    if (position < 0 || position > this->max_pos) {
        Serial.print("Cannot move to pos: ");
        Serial.println(position);
        return;
    }

    if (MyStepper::at_goal()) {
        this->goal = static_cast<uint32_t>(position * steps_per_mm);
        int32_t to_move = this->goal - this->position;
        if (to_move != 0) {
            this->_at_goal = false;
            A4988::startMove(to_move * this->direction);
        }
    }
}

/*
 * Get the current position in mm
 */
double MyStepper::get_position_mm() {
    return this->position * this->steps_per_mm;
}

// For homing
volatile bool MyStepper::home_switch = false;
uint32_t MyStepper::home_switch_pin = END_STOP;
void home_interrupt()
{
    MyStepper::home_switch = !digitalRead(MyStepper::home_switch_pin);
}