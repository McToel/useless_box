#ifndef MYSTEPPER_H
#define MYSTEPPER_H

#include "A4988.h"
#include "config.h"

using namespace std;

class MyStepper : public A4988
{
public:
    MyStepper(
        double steps_per_mm, short direction, uint32_t home_switch_pin,
        uint32_t steps, double rpm, double accel, double decel, double max_pos,
        uint32_t dir, uint32_t step, uint32_t ms1, uint32_t ms2,
        uint32_t ms3
    );

    bool at_goal();

    void home();

    void begin();

    void move_to(double position);

    double get_position_mm();
    static volatile bool home_switch;
    static uint32_t home_switch_pin;

private:
    double steps_per_mm;
    uint32_t position;
    uint32_t goal;
    bool _at_goal = true;
    bool homed = false;
    float rpm;
    double accel;
    double decel;
    short direction = 1;
    double max_pos = 0;
};

void home_interrupt();

#endif